import Head from 'next/head'

export default function PageMeta({ title, description }) {
    const siteName = process.env.NEXT_PUBLIC_APP_NAME
    const fullTitle = title ? `${title} | ${siteName}` : siteName
    const desc = description ? description : 'Hello World App'

    return (
        <Head>
            <title>{fullTitle}</title>
            <meta name="description" content={desc} />
        </Head>
    )
}