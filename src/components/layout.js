import { Roboto_Serif } from 'next/font/google'

const roboto_serif = Roboto_Serif({ subsets: ['latin'] })

export default function Layout({ children }) {

  return <div className={roboto_serif.className}>{children}</div>

}
