import React, { useState, useContext } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import Layout from '@/components/layout'
import PageMeta from '@/components/page-meta'
import { UserInfo } from '@/contexts/user-info'

const FormPage = () => {
    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [errors, setErrors] = useState({});
    const { addData } = useContext(UserInfo)
    const router = useRouter()

    const handleSubmit = (e) => {
        e.preventDefault();
        let formErrors = {};
        if (!name) formErrors.name = 'Name is required';
        if (!age) formErrors.age = 'Age is required';
        else if (!Number.isInteger(parseInt(age))) formErrors.age = 'Age must be a valid number';
        setErrors(formErrors);
        if (Object.keys(formErrors).length === 0) {
            addData({ name, age });
            setName('');
            setAge('');
            router.push('/data');
        }
    };

    return (
        <Layout>

            <PageMeta
                title="Submit a Request"
                description="Use this form to submit a request with your name and age to our Hello World app built with Next.js."
            />

            <div className="container">
                <h1 className="title">Submit a Request</h1>
                <form onSubmit={handleSubmit}>
                    <div className="field">
                        <label htmlFor="name" className="label">
                            Name:
                        </label>
                        <input
                            type="text"
                            id="name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            className={`input ${errors.name ? 'is-danger' : ''}`}
                        />
                        {errors.name && <p className="help is-danger">{errors.name}</p>}
                    </div>
                    <div className="field">
                        <label htmlFor="age" className="label">
                            Age:
                        </label>
                        <input
                            type="text"
                            id="age"
                            value={age}
                            onChange={(e) => setAge(e.target.value)}
                            className={`input ${errors.age ? 'is-danger' : ''}`}
                        />
                        {errors.age && <p className="help is-danger">{errors.age}</p>}
                    </div>
                    <button type="submit" className="button">
                        Submit
                    </button>
                    <br />
                </form>
                <div className="buttons">
                    <Link href="/" className="button">
                        Go to Home
                    </Link>
                    <Link href="/data" className="button">
                        Go to Data
                    </Link>
                </div>
            </div>
        </Layout>
    )
}

export default FormPage