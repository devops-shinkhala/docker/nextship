import React, { useContext } from 'react'
import Link from 'next/link'
import Layout from '@/components/layout'
import PageMeta from '@/components/page-meta'
import { UserInfo } from '@/contexts/user-info'

const DataPage = () => {

    const { data } = useContext(UserInfo)
    return (
        <Layout>

            <PageMeta
                title="View Data"
                description="View a list of submitted data in our Hello World app built with Next.js. See the names and ages of users who have submitted requests."
            />

            <div className="container">
                <h1 className="title">View Data</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Age</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((item) => (
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.age}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div className="buttons">
                    <Link href="/" className="button">
                        Go to Home
                    </Link>
                    <Link href="/form" className="button">
                        Go to Form
                    </Link>
                </div>

            </div>
        </Layout>
    );
};

export default DataPage;