import Link from 'next/link'
import Layout from '@/components/layout'
import PageMeta from '@/components/page-meta'

export default function Home() {
    return (
        <Layout>

            <PageMeta
                title="Home"
                description="Welcome to our Hello World app built with Next.js. Submit a request with your name and age or view a list of submitted data."
            />

            <div className="container">
                <h1 className="title">Welcome to {process.env.NEXT_PUBLIC_APP_NAME}!</h1>
                <p className="subtitle">
                    This is a simple Hello World app built using Next.js. You can use this app to submit a request or view data.
                </p>
                <div className="buttons">
                    <Link href="/form" className="button">
                        Submit a request
                    </Link>
                    <Link href="/data" className="button">
                        See the data
                    </Link>
                </div>
            </div>
        </Layout>
    )
}