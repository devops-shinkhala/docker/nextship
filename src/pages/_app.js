import { DataProvider } from '@/contexts/user-info'
import '@/styles/styles.css'

function MyApp({ Component, pageProps }) {
    return (
        <DataProvider>
            <Component {...pageProps} />
        </DataProvider>
    )
}

export default MyApp
