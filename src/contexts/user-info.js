import { createContext, useState } from 'react'

export const UserInfo = createContext()

export function DataProvider ( { children } ) {
    const [data, setData] = useState( [
        { id: 1, name: 'John Doe', age: 25 },
        { id: 2, name: 'Jane Smith', age: 30 },
        { id: 3, name: 'Bob Johnson', age: 35 },
    ] )

    const addData = ( newData ) => {
        setData( ( prevData ) => {
            // Calculate the next ID value
            const nextId = prevData.reduce( ( maxId, item ) => Math.max( maxId, item.id ), 0 ) + 1

            // Add the new data with the calculated ID
            return [...prevData, { ...newData, id: nextId }]
        } )
    }

    return (
        <UserInfo.Provider value={{ data, addData }}>
            {children}
        </UserInfo.Provider>
    )
}
