# NextShip: A Hello World Next.js App

![App Screenshot](public/img/screenshot.png)

## Introduction
This is a simple Hello World app built using Next.js. The app allows users to submit a request with their name and age, and view a list of submitted data. It's a great starting point for anyone looking to learn about Next.js and its features.

To see a full tutorial on how to dockerized this nextjs 13 app, visit the [A Step-by-Step Guide to Dockerize Your Next.JS Application](https://www.webhat.in/?p=859) blog article.

## Table of Contents
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Prerequisites
Before you get started, make sure you have the following installed on your system:
- Node.js (version 12.22 or later)
- npm (version 8.5 or later)
- Docker

## Installation
To install the app, follow these steps:
1. Clone this repository to your local machine.
2. Navigate to the project directory and run `npm install` to install all dependencies.
3. Run `npm run dev` to start the development server.

## Usage
Once the development server is running, you can access the app by navigating to `http://localhost:3000` in your web browser.

If you prefer running the app using Docker, make sure you have Docker installed on your system. Then, follow these steps:
1. Build the Docker image by running the command `docker build -t nextship-app .` in the project directory.
2. Run a Docker container based on the built image using the command `docker run -p 3000:3000 nextship-app`.
3. Access the app by visiting `http://localhost:3000` in your web browser.

## Contributing
If you'd like to contribute to this project, feel free to submit a pull request or open an issue.

## License
This project is licensed under the MIT License.